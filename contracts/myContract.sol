pragma solidity 0.5.0;

contract MyContract {
    address public owner;
	Transaction[] history;
	Client[] public clients;
	
	//mapping(address => uint) public clients;
	// call (affiche l'état) / send transaction
	// pure (no read no write) / view (read no write)
  
    struct Client {
		address owner; //could need "payable" : an address you can send Ether to, send + transfer members
		int balance;
        }
	
	struct Transaction {
		Client sender;
		Client receiver;
		int amount;
		string title;
		}
	
    constructor() public {
		owner = msg.sender;
        }
	
	function login() public {
		address msgSender = msg.sender;
		for (uint i = 0; i < clients.length; i++) {
				if (clients[i].owner == msgSender) {
					return ;
				}
			}
		clients.push(Client({
                owner: msg.sender,
                balance: 0
            }));
		return ;
	}
	
	function logWith(address a) public {
		address msgSender = a;
		for (uint i = 0; i < clients.length; i++) {
				if (clients[i].owner == msgSender) {
					return ;
				}
			}
		clients.push(Client({
                owner: msg.sender,
                balance: 0
            }));
		return ;
	}
	
	
	function getClientsLength() public view returns (uint) {
		return clients.length;
	}
	
	function getClientAt(uint indice) public view returns (address) {
		return clients[indice].owner;
	}
	
	function getBalanceAt(uint indice) public view returns (int) {
		return clients[indice].balance;
	}
	
	function transactionTo(string memory title, address sender, address receiver, int amount) public {
		Client memory s;
		Client memory r;
		for (uint i = 0; i < clients.length; i++) {
				if (clients[i].owner == sender) {
					clients[i].balance = clients[i].balance - amount;
					s = clients[i];
				}
				if (clients[i].owner == receiver) {
					clients[i].balance = clients[i].balance + amount;
					r = clients[i];
				}
			}
		history.push(Transaction({
					sender : s,
					receiver : r,
					amount : amount,
					title : title
					}));
	}
	
	function getLastTransactionTitle() public view returns(string memory) {
		uint size = history.length;
		return history[size-1].title;
	}
	function getLastTransactionSender() public view returns(address) {
		uint size = history.length;
		return history[size-1].sender.owner;
	}
	function getLastTransactionReceiver() public view returns(address) {
		uint size = history.length;
		return history[size-1].receiver.owner;
	}
	function getLastTransactionAmount() public view returns(int) {
		uint size = history.length;
		return history[size-1].amount;
	}
}

// Une fois sauvegardé :
// truffle compile
// truffle migrate (--reset)