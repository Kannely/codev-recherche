// voir : https://developer.mozilla.org/fr/docs/Glossaire/IIFE
// permet de séparer les portées (sinon toute variable a une portée globale)
(() => {
    // voir : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';
    // La flèche est une syntaxe pour une fonction fléchée
    // voir : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Fonctions_fl%C3%A9ch%C3%A9es
    // c'est une fonction qui prend en paramètre un objet (ici web3) et effectue un traitement dessus
    initialiseWeb3().then((web3) => {
        // C'est ici qu'on pourrait mettre notre code.
		// document.write() à ne pas utiliser
		
		console.log("Consultable avec F12");
		
		// Au chargement de la page ----------------------------------
		document.querySelector('#currentAccount').innerHTML = web3.eth.defaultAccount;
		
		// Action Login ----------------------------------------------
		document.querySelector('#login-button').onclick = (() => {
			callSmartContractFunction('MyContract.json', web3Provider, (contractInstance) => {
				contractInstance.login.sendTransaction().then((message) => {
					console.log("[Login]Transaction sent.");
				});
				
			});	
		});
		
		document.querySelector('#login-refresh').onclick = (() => {
			callSmartContractFunction('MyContract.json', web3Provider, (contractInstance) => {
				var nbClients = 0;
				contractInstance.getClientsLength.call().then((message) => {
					console.log(message);
					nbClients = message;
				var i;
				for (i = 0; i<nbClients; i++) {
					updateClientView(i,contractInstance);
				}
				});
			});	
		});
		
		
		// Action ajout de transaction -------------------------------
		document.querySelector('#add-button').onclick = (() => {
			callSmartContractFunction('MyContract.json', web3Provider, (contractInstance) => {
				var title = document.getElementById('TRtitle').value;
				var paidBy = document.getElementById('TRpaidby').value;
				var receiver = document.getElementById('TRreceiver').value;
				var amount = document.getElementById('TRamount').value;
				contractInstance.transactionTo.sendTransaction(title,paidBy,receiver,amount).then((message) => {
					console.log("[Transaction]Transaction sent.");
				});
			});	
		});
		
		// Action rafraichir le compte courrant ----------------------
		document.querySelector('#refresh').onclick = (() => {
			document.querySelector('#currentAccount').innerHTML = web3.eth.defaultAccount;
		});		
		
		// Action rafraichir l'historique ----------------------------
		document.querySelector('#refreshHistory').onclick = (() => {
			callSmartContractFunction('MyContract.json', web3Provider, (contractInstance) => {
				contractInstance.getLastTransactionTitle.call().then((message) => {
					document.querySelector('#exp1Title').innerHTML = message;
				});
				contractInstance.getLastTransactionSender.call().then((message) => {
					document.querySelector('#exp1Sender').innerHTML = message;
				});
				contractInstance.getLastTransactionReceiver.call().then((message) => {
					document.querySelector('#exp1Receiver').innerHTML = message;
				});
				contractInstance.getLastTransactionAmount.call().then((message) => {
					document.querySelector('#exp1Amount').innerHTML = message;
				});
			});	
		});
		
		
		// Appel du contrat, necessite de trouver comment avoir une interface json
		//web3.eth.Contract(jsonInterface, address, options)
		
		
        // voir : https://developer.mozilla.org/fr/docs/Web/API/Document/querySelector
        // voir : https://developer.mozilla.org/fr/docs/Web/API/Element/innertHTML
        //document.querySelector('#userId').innerHTML = web3.eth.defaultAccount;
    });
})();

function updateClientView(i,contractInstance) {
	var indice = i+1;
	contractInstance.getClientAt.call(i).then((message) => {			
		var accTag = '#accountID' + indice;
		document.querySelector(accTag).innerHTML = message;
	});
	contractInstance.getBalanceAt.call(i).then((message) => {			
		var balTag = '#balance' + indice;	
		console.log('Balance refresh neg?' + message);
		document.querySelector(balTag).innerHTML = message + '  ETH ';
	});
}